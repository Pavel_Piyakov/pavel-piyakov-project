"use strict";

//Отредактируйте данный файл и добавьте в объект товара:
//●массив ссылок на все изображения товара (берем изhtml)
//●массив строк всех возможных цветов у данного товара
//●массив строк всех возможных конфигураций памяти
//●массив возможных вариантов доставки, где каждыйвариант доставки описан в виде объекта

/**Объект описывает характеристики товара */
let itemCharacteristic = {
    itemName: "Apple iPhone 13",
    itemColor: "Cиний",
    itemMemory: "128 ГБ",
    itemDescriptionScreen: "6.1",
    itemDescriptionOS: "iOS 15",
    itemDescriptionWirelessInterface: 'NFC, Bluetooth, Wi-Fi',
    itemDescriptionCore: "Apple A15 Bionic",
    itemDescriptionWeight: "173 г",
    itemNewCost: "67990₽",
    itemOldCost: "75000₽",
    itemSale: "-8%",
};

/**Объект описывает характеристики первого отзыва */
let review1 = {
    reviewerName: "Марк.Г",
    reviewerRate: 5,
    reviewerPhoto: "./resources/review-1.svg",
    reviewerUsageExperience: "менее месяца",
    reviewerPositive: `это мой первый айфон после после огромного количества телефонов на андроиде. всё 
    плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,ширик тоже на высоте.`,
    reviewerNegative: `к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) 
    а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное`,
};

/**Объект описывает характеристики второго отзыва */
let review2 = {
    reviewerName: "Валерий Коваленко",
    reviewerRate: 4,
    reviewerPhoto: "./resources/review-2.svg",
    reviewerUsageExperience: `менее месяца`,
    reviewerPositive: `OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго.`,
    reviewerNegative: `Плохая ремонтопригодность`,
};

/**Массив всех фотографий товара */
let allItemImg = [
    './resources/image-1.webp',
    './resources/image-2.webp',
    './resources/image-3.webp',
    './resources/image-4.webp',
    './resources/image-5.webp',
    './resources/image-6.webp'
];

/**Массив цветов товара */
let allColors = [
    {color: 'Red', src: './resources/color-1.webp'},
    {color: 'Green', src: './resources/color-2.webp'},
    {color: 'Pink', src: './resources/color-3.webp'},
    {color: 'Blue', src: './resources/color-4.webp'},
    {color: 'White', src: './resources/color-5.webp'},
    {color: 'Black', src: './resources/color-6.webp'}
];

/**Массив вариантов объема памяти */
let allMemoryVolume = [128, 256, 512];

/**Массив вариантов доставки */
let allTypesOfDelivery = [
    {type: 'Самовывоз', date: 'четверг, 1 сентября', cost: 'бесплатно'},
    {type: 'Курьером', date: 'четверг, 1 сентября', cost: 'бесплатно'}
];