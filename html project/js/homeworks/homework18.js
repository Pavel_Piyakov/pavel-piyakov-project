"use strict";
let HW18;
HW18 = `-----------Homework18-----------`;
console.log(HW18);

// Задание 1
console.log('           Задание 1')

let a='100px';
let b='323px';
console.log(parseInt (a) + parseInt (b));


// Задание 2
console.log('           Задание 2')

console.log(Math.max(10, -45, 102, 36, 12, 0, -1));


// Задание 3
console.log('           Задание 3')

let c = 123.3399;                            // Округлить до 123
// Решение
console.log(Math.round(c));

let d=0.111;                                 // Округлить до 1
// Решение
console.log(Math.ceil(d));

let e=45.333333;                             // Округлить до 45.3
// Решение
console.log(e.toFixed(1));

let f=3;                                     // Возвести в степень 5 (должно быть 243)
// Решение
console.log(f**5);

let g=400000000000000;                       // Записать в сокращенном виде
// Решение
console.log(4e14);

//let h='1'!=1;                                // Поправить условие, чтобы результат был true (значения изменять нельзя, только оператор)
// Решение
let h='1'==1; 
console.log(h);


// Задание 4
console.log('           Задание 4')

console.log(0.1+0.2===0.3);
console.log(`при переводе в двоичную систему исчисления образуется остаток и отсюда образуется погрешность. строгое равно не будет исполняться как "true" при погрешности.`);
// при переводе в двоичную систему исчисления образуется остаток и отсюда образуется погрешность. строгое равно не будет исполняться как "true" при погрешности.