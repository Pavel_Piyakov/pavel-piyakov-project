"use strict";
let HW21;
HW21 = `-----------Homework21-----------`;
console.log(HW21);

// Задание 1
// Напишите функциюisEmpty(obj), которая возвращает true,если у объекта нет свойств,
// иначе false. Должна корректноработать абсолютно для любого объекта. Добавьте для
// данной функции комментарий в виде JSDoc с описанием того, что она
// делает, какие параметры принимает и что возвращает

/** Фукция isEmpty(obj) проверяет наличие свойств у объекта
 * @param {object} obj - объект для проверки свойств
 * @return {boolean} //false - если есть свойства ; //true - если свойств нет
*/
let obj = {};
function isEmpty(obj) {
    for (let prop in obj) {
        return false;
    }
    return true; 
}

// Задание 3
// Необходимо написать функцию raiseSalary(perzent),которая позволит произвести повышение
// зарплаты на определенный процент и будет возвращать объект с новыми зарплатами.Например,
// если мы передаем внутрь этой функции число 5, то зарплата каждого сотрудника должна быть
// повышена на 5%(применить округление до целого числа в меньшую сторону).После чего
// необходимо вывести в консоль общий бюджетнашей команды, т.е. суммарное значение всех
// зарплат послеизменения.

/** Функция  raiseSalary() принимает параметр perzent и увеличивает зп в %
 * @param {number} perzent - процент увеличения зп
 * @param {number} newSalary - зп после индексации
 * @param {number} summSalary - сумма всех ЗП
 * @return {number} - возвращает сумму зп без цифр после запятой)
 */

let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

function raiseSalary(perzent) {
    let newSalary = 0;
    let summSalary = 0;
// для каждого значения в salaries необходимо *процент/100 и прибавит изначальное количество
    for (let props in salaries) {
        newSalary = (salaries[props]*perzent/100) + salaries[props];
        summSalary = summSalary + newSalary;  
    }
    return parseInt(summSalary);
} 

console.log(raiseSalary(5));