"use strict";
let HW22;
HW22 = `-----------Homework22-----------`;
console.log(HW22);

// Задание 1
console.log('           Задание 1')
//Напишите функциюgetSumm(arr), которая принимает любоймассив и 
//возвращает сумму чисел в этом массиве. Обратитевнимание, что в
// качестве элементов массива могут приходитьне только числа и 
//такие элементы необходимо пропускать.

/** Функция getSumm(arr) считает сумму всех чисел в массиве, не учитывая то, что не является числами
 * @type {any[]} arr - массив с данными для сложения
 * @return {number[]} arrNumbers - массив состоящий только из чисел
 * @param {number} summarize - сумма чисел в массиве
*/

let arr = [1, 2, 10, 5, , "ffff", {}, , -33];

function getSumm(arr) {
  let arrNumbers = arr.filter(function (n) {
    return Number.isFinite(n);
  }
);
  let summarize = 0;
    for (let n in arrNumbers) {
        summarize = summarize + Number(arrNumbers[n]);
    }
  return summarize;
}

console.log('Массив:');
console.log(arr);
console.log('Сумма чисел в массиве: ');
console.log(getSumm(arr));

// Задание 3
console.log('           Задание 3')
// Реализуйте упрощенную корзину в интернет-магазине.Представим, что мы храним
// в корзине идентификаторы товаров, которые пользователь добавил в корзину и 
// нам нужно реализовать две функции: добавить в корзину и удалить изкорзины. 
// Причём в корзину нельзя добавить два одинаковыхтовара.

let cart = [3333, 3334, 3335]

/** Функция addToCart добавляет товар в корзину
 * @param {any[]} cart - массив с идентификаторами товаров в корзине до изменений
 * @return {any[]} cart - массив корзина после изменения 
 */

function addToCart(ID) {
  if (cart.includes(ID) === false) {
    cart.push(ID);
  }
  return cart;
}

/** Функция removeFromCart удаляет товар из корзины
 * @param {any[]} cart - массив с идентификаторами товаров в корзине до изменений
 * @param {number} index - индекс товара, который нужно удалить из корзины
 * @return {any[]} cart - массив корзина после изменения 
 */

function removeFromCart(ID) {
  let index = cart.indexOf(ID); 
  if (index >= 0) {
      cart.splice(index, 1);  
  }
  return cart;
}

console.log('В корзине имеются сделающие товары: ');
console.log(cart);

console.log('Добавляем в корзину товары 3335, 3336 и удаляем товар 3333 ');
addToCart(3335);
addToCart(3336);
removeFromCart(3333);

console.log(cart);