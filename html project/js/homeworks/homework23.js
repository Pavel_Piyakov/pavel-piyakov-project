"use strict";
let HW23;
HW23 = `-----------Homework23-----------`;
console.log(HW23);

// Задание 1
console.log('           Задание 1'); 
//Напишите таймер обратного отсчёта. При открытии страницы со
// скриптом необходимо спросить у пользователя число и запустить
// таймер обратного отсчёта, который, начиная с этого числа,каждую 
//секунду будет уменьшать число на единицу ивыводить в консоль 
//сообщение “Осталось n”, где n это данное число. Как только число 
//станет равно нулю, необходимо остановить таймер и вывести в 
//консоль “Время вышло!”

//Необходимо учесть в коде, что пользователь может ввести не число
// (выводим ошибку в консоль, текст наваше усмотрение)

//Попробуйте решить задачу с помощью Promise

let promiseTimer = new Promise(function(resolve) {
  setTimeout(function() {
    let start = prompt('Введите число для старта таймера');

    if (isNaN(start)) {
      console.log('Вы ввели не число');
    } 
    else {
      let tick = setInterval(() => {
        console.log('Осталось ' + start); 
        start --;
        if(start===0) {
          clearInterval(tick);
          setTimeout(() => {
            console.log('Время вышло!');
          }, 1000);
        }
      }, 1000); 
    }
  },0);
});



// Упражнение 2  
console.log('           Задание 2') 
//Выполните запрос на бэкенд по адресу https://reqres.in/api/users
//и выведите в консоль количествопользователей в ответе, а также имя,
// фамилию и email каждого пользователя в таком формате (соблюдение формата обязательно):

//отправляем фетч запрос бекенду - протокол изменен с https на http потому что у меня постоянно 
//вылетала ошибка сервера черед девтулз. через постман все Ок
let promise = fetch("https://reqres.in/api/users"); //передаю в переменную адрес бэкэнда с помощью функции fetch

//время начала запроса
let timeStart = Number(new Date());

promise
//переводим ответ в JSON
.then(function (response) {
  return response.json();
}) 

//Ответ бекенда
.then(function(response) {

//извлекаем данные о пользователях из массива data  
  let users = response.data;
  console.log(response);

//выводим кол-во пользователей
  console.log("Получили пользователей: " + users.length);
  for (let key in users) {
      console.log(`- ${users[key]['first_name']} ${users[key]['last_name']} (${users[key]['email']})`)
  }
//Получаем время окончания выполнения запроса в милисекундах
  let timeEnd = Number(new Date());
  //console.log(dateEnd);
  console.log('Время обработки запроса: ' + ((timeEnd - timeStart) + 'ms'));

}) 

//выполняется в случае ошибки
.catch(function() {
  console.log("Кажется бэкенд сломался :(");
});
