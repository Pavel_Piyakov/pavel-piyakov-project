"use strict";
/**
 * В двух предыдущих домашних заданиях мы работали с формой иписали
 *  для неё валидацию данных и сохранение вlocalStorage.Необходимо
 *  создать новый файлpage-product-oop.jsи переписатьуже существующий
 *  код в ООП-стиле, создав, к примеру, классAddReviewFormи добавив в
 *  него все необходимые свойстваиметоды.
 * 
 * Дополнительно:
 * Давайте представим что у нас на сайте 10 разных форм и у каждойиз них может быть своя логика
 *  валидации, но после отправки онивсе по умолчанию вызывают alert с сообщением 
 * “Формауспешноотправлена!”Попробуйте реализовать классForm, который будет 
 * хранитьобщийдля всех форм код, а также классAddReviewFormнаследующийFormи
 *  добавляющий свою логику валидации.
 */

// Создаем общий класс Form для каждой будущей формы
class Form {

    fields = {};
    
    eachForm = document.querySelector('.forms');

    eachInput = this.eachForm.querySelectorAll('.inputs');

    //Создаем методы для ошибок и подтверждения  валидности формы
    showErrorForInput = (input, errorText) => {
        let inputError = input.parentElement.querySelector('.inputErrors');
        inputError.style.visibility="visible";
        inputError.textContent = errorText;
    };

    hideErrorForInput = (input) => {
        let inputError = input.parentElement.querySelector('.inputErrors');
        inputError.style.visibility="hidden";
    };

    formIsValid = () => {
        return true;
    };
 
    // задаем общий метод для валидации формы для каждого инпута
    formValidation = () => {
        //сохраняем данные куков
        this.eachForm.addEventListener('input', (event) => {
            let { target } = event;
            let { id, value } = target;
            this.fields[id] = value;
            localStorage.setItem(id, value);
        });

        this.eachInput.forEach((input) => {
            this.fields[input.id] = localStorage.getItem(input.id);
            input.value = this.fields[input.id];
            //создаем ивент на скрытие ошибки при повторном вводе данных в инпут            
            input.addEventListener('focus', () => {
                this.hideErrorForInput(input);
            });
        });

        //создаем ивент на подтверждение формы
         this.eachForm.addEventListener('submit', (event) => {

             event.preventDefault();
             
             if (this.formIsValid()) {
                 this.eachInput.forEach((field) => {

                    localStorage.removeItem(field.id);
                    field.value = '';
                });
                alert("Форма успешно отправлена!");
                };
         });
    };

 
    resetFormFieldErrors = () => {
         this.eachInput.forEach((input) => {
             this.hideErrorForInput(input)
         });
    };
};



class AddReviewForm extends Form {

    eachForm = document.querySelector('.newReview');
 
    validationForNameInput = () => {
        let input = this.eachForm.querySelector('.new-review__name-input');
        let inputValue = input.value;
         
        if (!inputValue) {
            this.showErrorForInput(input, 'Вы забыли указать имя и фамилию');
        } else if (inputValue.length < 2) {
            this.showErrorForInput(input, 'Имя не может быть короче 2-х символов');
        } else {
            this.hideErrorForInput(input);
            return true;
        }
 
        return false;
    };
 
    validationForRatingInput = () => {
        let input = this.eachForm.querySelector('.new-review__rating-input');
        let inputValue = +input.value;
 
        if (!inputValue 
            || Number.isNaN(inputValue)
            || inputValue < 1 || inputValue > 5) {
            this.showErrorForInput(input, 'Оценка должна быть от 1 до 5');
        } else {
            this.hideErrorForInput(input);
            return true;
        };
 
        return false;
    };
 
    formIsValid = () => {
        this.resetFormFieldErrors();
        if (this.validationForNameInput()) {
            if (this.validationForRatingInput()) return true;
        };
 
        return false;
    };
};

let reviewForm = new AddReviewForm();
reviewForm.formValidation();