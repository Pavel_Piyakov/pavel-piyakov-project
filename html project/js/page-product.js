"use strict";
//Добавление нового комментария

let addReview = document.getElementById("addReview");
let submitButton = document.querySelector("reviewbutton");

let reviewNameInput = document.getElementById("new-review__name-input");
let reviewNameInputValue = reviewNameInput.value;

reviewNameInput.value = localStorage.getItem('userNameCookie');

let errorForEmptyInput = 'Вы забыли указать имя и фамилию';
let errorForLessThan2Characters = 'Введено меньше 2 символов';

let errorForNameInput = document.getElementById("error-name-input");
errorForNameInput.hidden = true;

reviewNameInput.addEventListener("input", (event) => {
    reviewNameInputValue = event.target.value;
    localStorage.setItem('userNameCookie', reviewNameInputValue);
 });
 
let validationForNameInput = function () {
    if (!reviewNameInputValue) {
        showErrorForNameInput(errorForEmptyInput);
        return false;
    }

    if (reviewNameInputValue.length < 2) {
        showErrorForNameInput(errorForLessThan2Characters);
        return false;
    }

    hideErrorForNameInput();
    return true;
};

let showErrorForNameInput = function (errorMessage) {
    let errorForNameInput = document.getElementById('error-name-input');
    errorForNameInput.innerText = errorMessage;
    errorForNameInput.hidden = false;
}

let hideErrorForNameInput = function () {
    let errorForNameInput = document.getElementById('error-name-input');
    errorForNameInput.hidden = true;
}

let reviewRatingInput = document.getElementById('new-review__rating-input');
let reviewRatingInputValue = reviewRatingInput.value;

let errorForRating = 'Оценка должна быть от 1 до 5';

reviewRatingInput.value = localStorage.getItem('userRatingCookie');
reviewRatingInput.addEventListener("input", (event) => {
    reviewRatingInputValue = event.target.value;

    localStorage.setItem('userRatingCookie', reviewRatingInputValue);
});


let validationForRatingInput = function() {
    if (!reviewRatingInputValue
        || !Number.isFinite(+reviewRatingInputValue)
        || (1 > reviewRatingInputValue || 5 < reviewRatingInputValue)
    ) 
        {showErrorForRatingInput(errorForRating);
        return false;
    }
    hideErrorForRatingInput();
    return true;
}

let showErrorForRatingInput = function(errorMessage) {
    let errorForRatingInput = document.getElementById('error-rating-input');
    errorForRatingInput.innerText = errorMessage;
    errorForRatingInput.hidden = false;
}

let hideErrorForRatingInput = function() {
    let errorForRatingInput = document.getElementById('error-rating-input');
    errorForRatingInput.hidden = true;
}

let validateForm = () => {
    let isReviewNameInputValid = validationForNameInput();
    let isReviewRatingInputValid = validationForRatingInput();
    localStorage.removeItem('userNameCookie');
    localStorage.removeItem('userRatingCookie');
    return isReviewNameInputValid && isReviewRatingInputValid;
};



//Добавление товара в корзину

let addToCartButton = document.querySelector(".important__button");
let countOfItemsInCart = document.querySelector(".header__cart-counter");
let cartCounterBackground = document.querySelector(".header__cart-orangeRound");

countOfItemsInCart.innerHTML = localStorage.getItem('CountItemsInCart'); 
addToCartButton.addEventListener("click", function() { 

    if (!localStorage.getItem('CountItemsInCart')) {
        countOfItemsInCart.innerHTML = "1";
        localStorage.setItem("CountItemsInCart", countOfItemsInCart.innerHTML);
        cartCounterBackground.style.display = "block";
        addToCartButton.innerHTML = "Товар добавлен в корзину";
    }

    else {
        localStorage.removeItem('CountItemsInCart');
        cartCounterBackground.style.display = "none";
        countOfItemsInCart.innerHTML = ""; 
        addToCartButton.innerHTML = "Добавить в корзину";
    }
});

if (localStorage.getItem('CountItemsInCart')) {
    cartCounterBackground.style.display = "block"; 
    addToCartButton.innerHTML = "Товар добавлен в корзину";
 };
