import React from 'react';
import { BrowserRouter, Routes, Route} from "react-router-dom";
import Header from'./components/Header/Header.jsx';
import PageProduct from'./components/PageProduct/PageProduct.jsx';
import PageMain from './components/PageMain/PageMain.jsx';
import Footer from'./components/Footer/Footer.jsx';

function App() {
  return (
    <BrowserRouter>
      <Routes>
          <Route path="/" element={
            <>
            <Header />
            <PageMain />
            <Footer />
            </>} />

          <Route path="/product" element={
            <>
            <Header />
            <PageProduct />
            <Footer /> 
            </>} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
