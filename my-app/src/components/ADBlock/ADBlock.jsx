import './ADBlock.css';
import Adb from '../Adb/Adb';

function ADBlock() {
    return (
        <div className="advertising">
            <div className="advertising__text">
                Реклама
            </div>

            <div className="advertising__list">
                <Adb src="./adbbanner1.html" />
                <Adb src="./adbbanner2.html" />
            </div>
        </div>
    )
}

export default ADBlock