import React from 'react';
import styles from"./Footer.module.css";
import Link from '../Link/Link'
import { useCurrentDate } from "@kundinos/react-hooks";

function Footer() {
    const currentDate = useCurrentDate();
    const fullYear = currentDate.getFullYear( {every: "hour"} );

    return ( 
        <div className={styles.container}>
            <div className={styles.footer}>
                <div className={styles.copyright}>
                        <div>
                            <b>© ООО «<span className={styles.orangeText}>Мой</span>Маркет», 2018-{`${fullYear}`}.</b><br/>
                            Для уточнения информации звоните по номеру <Link href="tel:79000000000"> +7 900 000 0000</Link>, <br />
                            а предложения по сотрудничеству отправляйте на почту <Link href="mailto:partner@mymarket.com">partner@mymarket.com</Link>
                        </div>         
                </div>

                <div className={styles.uplink}>
                    <Link href="#top" >Наверх</Link>
                </div>
            </div>
        </div>
    );
}

export default Footer;