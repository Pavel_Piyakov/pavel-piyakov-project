import React from 'react';
import './Header.css';
import HeaderCart from './HeaderCart/HeaderCart';
import { Link } from "react-router-dom";
import HeaderWishlist from './HeaderWishlist/HeaderWishlist';

function Header() {
    return ( 
        <>
            <div className="header-container" name="top">
                <div className="header">
                        <Link to='/' className="nolink header-logo" relative="path">
                        <div className="header__logoicon">
                            <img  className="header__logoicon" src="/favicon.svg" alt="Логотип" />
                        </div>

                        <div className="header__title">
                            <span><span className="orangeText"> Мой</span>Маркет</span>
                        </div>
                        </Link>
                        <div className='header__icons'>
                        <HeaderWishlist />
                        <HeaderCart />
                    </div>

                </div>

            </div> 
        </>
    );
}

export default Header;