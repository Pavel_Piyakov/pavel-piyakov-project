import './HeaderCart.css';
import { useSelector } from 'react-redux';

function HeaderCart() {

    useSelector((store) => store.cart.products);
    let itemsInCartCheck = localStorage.getItem('itemsInCart');
    let cartValue = itemsInCartCheck;
    let check = cartValue > 0
    
    return(
        <div className="header__cart">
            <img className="header__cart-svg" src="/cartVector.svg" alt="Иконка корзины"/>

            <div className={` cart__noItem ${check ? 'cart__gotItem' : ''}`}>
                {cartValue}
            </div>
            
        </div>

)
}

export default HeaderCart;