import './HeaderWishlist.css';
import { useSelector } from 'react-redux';

function HeaderWishlist() {

    useSelector((store) => store.wishlist.products);
    let itemsInWishlistCheck = localStorage.getItem('itemsInWishlist');
    let wishlistValue = itemsInWishlistCheck;
    let check = wishlistValue > 0
    
    return(
        <div className="header__wishlist">
            <img className="header__wishlist-svg" src="/heartVector.svg" alt="Иконка списка желаний"/>

            <div className={`wishlist__noItem ${check ? 'wishlist__gotItem' : ''}`}>
                {wishlistValue}
            </div>
            
        </div>

)
}

export default HeaderWishlist;