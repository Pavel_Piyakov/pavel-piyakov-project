import './Link.css';

function Link(props) {
    const { className, href, target, children} = props;
    return (
        <a
            className={`${className} link`}
            href={href}
            target={target}
        >
            {children}
        </a>
    );
}

export default Link;