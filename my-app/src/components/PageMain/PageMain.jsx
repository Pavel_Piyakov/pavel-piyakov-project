import React from 'react';
import './PageMain.css';
import { Link } from "react-router-dom"

function PageMain() {
    return (
        <>
            <div className='main-container'>
                <div className='main-text'>
                    <div>Здесь должно быть содержимое главной страницы.</div>
                    <div>Но в рамках курса главная страница  используется лишь<br />
                    в демонстрационных целях</div>
                </div>

                <div>
                    <Link to='/product' className='link' relative="path">Перейти на страницу товара</Link>
                </div>
            </div>
        </>
    );
}

export default PageMain;