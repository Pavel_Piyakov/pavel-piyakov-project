import React from 'react';
import items from './BreadCrumbsArray';
import Link from '../../Link/Link';
import styled from "styled-components";

const Brdcrmb = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    font-family: Arial, Helvetica, sans-serif;
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;    
    order: 0;
`;

function BreadCrumbs() {
    return (
        <Brdcrmb>
            {items.map((item, index) => {
                index = item.id;
                return (
                    <div key={item.id}>
                        <Link href={item.href}> {item.breadCrumb} </Link>
                        {index !== items.length-1 && <span> &gt;&nbsp;</span>}
                    </div>
                );
            })}
        </Brdcrmb>
    );
}

export default BreadCrumbs;