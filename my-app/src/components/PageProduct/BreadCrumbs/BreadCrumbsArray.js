const items = [
    {id: 0, href: "electronics", breadCrumb: 'Электроника'},
    {id: 1, href: "smartphones-and-gadgets", breadCrumb: 'Смартфоны и гаджеты'},
    {id: 2, href: "mobile-phones", breadCrumb: 'Мобильные телефоны'},
    {id: 3, href: "apple", breadCrumb: 'Apple'}
]

export default items;
