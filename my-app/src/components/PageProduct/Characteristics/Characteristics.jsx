import React from "react";
import Link from "../../Link/Link";
import "./Characteristics.css"

function Characteristics() {
    return (
        <div className="characteristics__list">
            <ul>
                <li><span className="characteristics__list-mark">Экран: <b>6.1</b></span></li>
                <li><span className="characteristics__list-mark">Встроенная память: <b>128 ГБ</b></span></li>
                <li><span className="characteristics__list-mark">Операционная система: <b>iOS 15</b></span></li>
                <li><span className="characteristics__list-mark" >Беспроводные интерфейсы: <b>NFC, Bluetooth,</b><br className="mark-wrap"/><b> Wi-Fi</b></span></li>
                <li><span className="characteristics__list-mark">Процессор: <Link href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank"><b>Apple A15 Bionic</b></ Link></span></li>
                <li><span className="characteristics__list-mark">Вес: <b>173 г</b></span></li>
            </ul>
        </div>
    );
}

export default Characteristics;