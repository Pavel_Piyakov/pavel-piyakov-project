import './ColorButton.css';

function ColorButton(props) {
    const { className, onClick, children } = props;
    return (
        <div
            className={className}
            onClick={onClick}
        >

            {children}
            
        </div>
    );
}

export default ColorButton;