import React, { useState } from 'react';
import './Colors.css';
import ColorButton from './ColorButton/ColorButton';
import allColors from './ColorsArray';

function Colors() {
    let [chosenColor, setchosenColor] = useState(3);
    const titleName = allColors[chosenColor].color;

    return (
        <div className="itemcolor__choose">
            <div className="itemcolor">
                <div className="itemcolor__title">{`Цвет товара: ${titleName}`}</div> 
                <div className="itemcolor__choose">
                    {allColors.map((color, index) => {
                        index = color.id;
                        let chosen = index === chosenColor;

                        return (
                            <ColorButton
                            className={`itemcolor__choosecolor ${chosen ? 'itemcolor__chosenColor' : ''}`}
                            onClick={() => setchosenColor(index)}
                                key={color.id}
                            >
                                <img className='itemcolor__img ' src={color.src} alt={color.alt} />
                            </ColorButton>

                        );
                    })}
                </div>
            </div>
        </div>
    );
}

export default Colors;

             