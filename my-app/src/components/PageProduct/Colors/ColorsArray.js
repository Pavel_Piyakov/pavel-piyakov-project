const allColors = [
    {id: 0, src: "./color-1.webp", color: 'Красный', alt: 'Красный'},
    {id: 1, src: "./color-2.webp", color: 'Зеленый', alt: 'Зеленый'},
    {id: 2, src: "./color-3.webp", color: 'Розовый', alt: 'Розовый'},
    {id: 3, src: "./color-4.webp", color: 'Синий', alt: 'Синий'},
    {id: 4, src: "./color-5.webp", color: 'Белый', alt: 'Белый'},
    {id: 5, src: "./color-6.webp", color: 'Черный', alt: 'Черный'}
];

export default allColors;
