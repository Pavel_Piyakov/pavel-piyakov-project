import React, { useState } from 'react';
import './Configs.css';
import ConfigsButton from './ConfigsButton/ConfigsButton';
import allMemoryConfigs from './ConfigsArray';

function Configs() {
    let [chosenConfig, setChosenConfig] = useState(0);
    const titleName = allMemoryConfigs[chosenConfig].memory;

    return (
        <div className="memory">
            <div className="memory__title">{`Конфигурация памяти: ${titleName}`}</div> 
            <div className="memory__choose">
                {allMemoryConfigs.map((memory, index) => {
                    index = memory.id;
                    let chosen = index === chosenConfig;

                    return (
                        <ConfigsButton
                        className={`memory__chooseConfig ${chosen ? 'memory__chosenConfig' : ''}`}
                        onClick={() => setChosenConfig(index)}
                            key={memory.id}
                        >
                        <div className='memory__configs'>{memory.memory}</div>
                        </ConfigsButton>
                    );
                })}
            </div>
        </div>
    );
}

export default Configs;