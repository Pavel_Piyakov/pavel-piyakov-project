const allMemoryConfigs = [
    {id: 0, memory: '128 ГБ'},
    {id: 1, memory: '256 ГБ'},
    {id: 2, memory: '512 ГБ'},
];

export default allMemoryConfigs;
