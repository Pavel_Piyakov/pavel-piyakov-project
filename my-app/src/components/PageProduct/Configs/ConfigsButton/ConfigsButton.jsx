import './ConfigsButton.css';

function ConfigsButton(props) {
    const { className, onClick, children } = props;
    return (
        <div
            className={className}
            onClick={onClick}
        >
            {children}
        </div>
    );
}

export default ConfigsButton;