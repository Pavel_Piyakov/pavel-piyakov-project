import React from 'react';
import './PageProduct.css';
import Colors from '../PageProduct/Colors/Colors';
import Configs from '../PageProduct/Configs/Configs';
import Reviews from '../PageProduct/Reviews/Reviews';
import Characteristics from '../PageProduct/Characteristics/Characteristics';
import PageTitle from '../PageProduct/PageTitle/PageTitle';
import Sidebar from '../PageProduct/Sidebar/Sidebar';
import Table from './Table/Table';


function PageProduct() {
    return ( 
        <>
            <PageTitle />
            <main>
                <div className="characteristics-container">
                    <div className="characteristics-main">
                        <Colors /> 
                        <Configs />

                        <div className="characteristics">

                            <div className="characteristics__title">
                                Характеристики товара
                            </div>

                            <Characteristics />
                        </div>   

                        <div className="description">
                            <div className="description__title">Описание</div>

                            <div className="description__paragrapf">
                                <div>
                                    Наша самая совершенная система двух камер. <br/>
                                    Особый взгляд на прочность дисплея. <br/>
                                    Чип, с которым всё супербыстро.<br/>
                                    Аккумулятор держится заметно дольше. <br/>
                                    <i>iPhone 13 - сильный мира всего.</i>
                                </div>

                                <div>                    
                                    Мы разработали совершенно новую схему расположения и развернули объективы на 45 градусов. Благодаря этому
                                    внутри корпуса поместилась наша лучшая система двух камер с увеличенной матрицей широкоугольной камеры. 
                                    Кроме того, мы освободили место для системы оптической стабилизации изображения сдвигом матрицы. И повысили 
                                    скорость работы матрицы на сверхширокоугольной камере.
                                </div>

                                <div>
                                    Новая сверхширокоугольная камера видит больше деталей в тёмных областях снимков. 
                                    Новая широкоугольная камера улавливает на 47% больше света для более качественных фотографий и видео. 
                                    Новая оптическая стабилизация со сдвигом матрицы обеспечит чёткие кадры даже в неустойчивом положении.
                                </div>

                                <div>
                                    Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещения фокуса и изменения резкости. 
                                    Просто начните запись видео. Режим «Киноэффект»будет удерживать фокус на объекте съёмки, создавая красивый эффект 
                                    размытия вокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другогочеловека или объект, 
                                    который появился в кадре. Теперь ваши видео будут смотреться как настоящее кино.
                                </div>   
                            </div>
                        </div>
                        
                        <Table />

                    </div>

                    <Sidebar />

                </div>
            </main>
            <Reviews/>
        </>
    );
}

export default PageProduct;