import React from 'react';
import BreadCrumbs from '../BreadCrumbs/BreadCrumbs';
import './PageTitle.css';

function PageTitle() {
    return ( 
        <>
            <div className="pagetitle">

                <BreadCrumbs />
                <div className="pagetitle__text">Смартфон Apple iPhone 13, синий</div>

                <div className="iphone-preview">
                    <div><img src="image-1.webp" className="iphone-preview__img" alt="Первая картинка"></img></div>
                    <div><img src="image-2.webp" className="iphone-preview__img" alt="Вторая картинка"></img></div>
                    <div><img src="image-3.webp" className="iphone-preview__img hide" alt="Третья картинка"></img></div>
                    <div><img src="image-4.webp" className="iphone-preview__img hide" alt="Четвертая картинка"></img></div>
                    <div><img src="image-5.webp" className="iphone-preview__img hide" alt="Пятая картинка"></img></div>
                </div>
            </div>
        </>
    );
}

export default PageTitle;