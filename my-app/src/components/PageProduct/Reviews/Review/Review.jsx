import './Review.css';

function Review(props) {    
    const { review} = props;
    const arrRating = Array(5).fill(1).fill(0, review.rating);

    return (
       <div> 
            <div className="reviewlist__rewiewD">
                <img className="reviewlist__photo" src={review.reviewerPhoto} alt="CustomerPhoto" />

                <div className="reviewlist__comment">
                    <div className="reviewlist__comment-header" key={review.reviewerName}>
                    <div className='reviewlist__comment-header'>{review.reviewerName}</div>

                    <div className="reviewlist__comment-rating">
                            {arrRating.map((rating, stars) => {
                                return (
                                    <div key={stars}>
                                        {rating ? <img className="reviewlist__comment-ratingstar"  src={review.ratingStarFill} alt="+"/> : <img className="reviewlist__comment-ratingstar" src={review.ratingStarEmpty} alt="-"/>}
                                    </div> 
                                );
                            })}
                    </div>

                    <div className="reviewlist__rewiew-title">
                        <div className="reviewlist__rewiew-title"><span>Опыт использования: <span className="reviewlist__rewiew-text">{review.exp}</span></span></div>
                        <div className="reviewlist__rewiew-title"><span>Достоинства: <div className="reviewlist__rewiew-text">{review.positive}</div></span></div> 
                        <div className="reviewlist__rewiew-title"><span>Недостатки: <div className="reviewlist__rewiew-text">{review.negative}</div></span></div>
                    </div>
                </div>
                </div>
            </div>

            <div className="reviewlist__rewiewM">
                <div className="reviewlist__comment">
                    <img className="reviewlist__photo" src={review.reviewerPhoto} alt="CustomerPhoto" />

                    <div className="reviewlist__comment-header" key={review.reviewerName}>
                        <div className='reviewlist__comment-header'>{review.reviewerName}</div>

                        <div className="reviewlist__comment-rating">
                                {arrRating.map((rating, stars) => {
                                    return (
                                        <div key={stars}>
                                            {rating ? <img className="reviewlist__comment-ratingstar"  src={review.ratingStarFill} alt="+"/> : <img className="reviewlist__comment-ratingstar" src={review.ratingStarEmpty} alt="-"/>}
                                        </div> 
                                    );
                                })}
                        </div>
                    </div>
                </div>

                <div className="reviewlist__rewiew-title">
                    <div className="reviewlist__rewiew-title"><span>Опыт использования: <span className="reviewlist__rewiew-text">{review.exp}</span></span></div>
                    <div className="reviewlist__rewiew-title"><span>Достоинства: <div className="reviewlist__rewiew-text">{review.positive}</div></span></div> 
                    <div className="reviewlist__rewiew-title"><span>Недостатки: <div className="reviewlist__rewiew-text">{review.negative}</div></span></div>
                </div>
            </div>
        </div>
    );
}

export default Review;

