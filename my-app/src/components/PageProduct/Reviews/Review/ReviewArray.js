const review = [
    {
        id: 0,
        reviewerName: "Марк Г.",
        reviewerPhoto: "./review-1.jpeg",
        rating: 5,
        ratingStarEmpty: "./zvzd2.svg",
        ratingStarFill: "./zvzd1.svg",
        exp: "менее месяца",
        positive: "это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, ширик тоже на высоте.",
        negative: "к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное"
    },

    {
        id: 1,
        reviewerName: "Валерий Коваленко.",
        reviewerPhoto: "./review-2.jpeg",
        rating: 4,
        ratingStarEmpty: "./zvzd2.svg",
        ratingStarFill: "./zvzd1.svg",
        exp: "менее месяца",
        positive: "OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго",
        negative: "Плохая ремонтопригодность"
        }
];

export default review;