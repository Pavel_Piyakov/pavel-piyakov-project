import {React, useState} from 'react';
import "./ReviewForm.css"

function ReviewForm() {

    const [reviewNameInput, setReviewNameInput] = useState (() => {
        return localStorage.getItem('inputName') || '';
    });

    function collectInputName(event) {
        let userName = event.target.value;
        localStorage.setItem('inputName', userName);
        setReviewNameInput(userName);
    }

    const [errorForNameInput, showErrorForNameInput] = useState (() => {
        return;
    });

    let errorForEmptyInput = 'Вы забыли указать имя и фамилию';
    let errorForLessThan2Characters = 'Введено меньше 2 символов';

///////////////////

    const [reviewRatingInput, setReviewRatingInput] = useState (() => {
        return localStorage.getItem('inputRating') || '';
    });

    function collectInputRating(event) {
        let userScore = event.target.value;
        localStorage.setItem('inputRating', userScore);
        setReviewRatingInput(userScore);
    }

    const [errorForRatingInput, showErrorForRatingInput] = useState (() => {
        return;
    });

    let errorForRating = 'Оценка должна быть от 1 до 5';

///////////////////

    const [reviewTextArea, setReviewTextArea] = useState (() => {
        return localStorage.getItem('inputText') || '';
    });
    
    function collectInputText(event) {
        let textAreaValue = event.target.value;
        localStorage.setItem('inputText', textAreaValue);
        setReviewTextArea(textAreaValue);
    }

///////////////////

    function handleChangeText() {
        showErrorForNameInput('');
        showErrorForRatingInput('');
    }

    function handleSubmit(event) {
        event.preventDefault();

        if (!reviewNameInput) {
            showErrorForNameInput(errorForEmptyInput);
        }

        else if (reviewNameInput.length === 1) {    
            showErrorForNameInput(errorForLessThan2Characters);
        }

        else if (reviewRatingInput < 1 
                || reviewRatingInput > 5 
                || isNaN(reviewRatingInput)) {
            showErrorForRatingInput(errorForRating);
        }

        else {
            localStorage.removeItem('inputName');
            setReviewNameInput('');

            localStorage.removeItem('inputRating');
            setReviewRatingInput('');

            localStorage.removeItem('inputText');
            setReviewTextArea('');

            alert('Ваш отзыв был успешно отправлен и будет отображён после модерации');
            return;   
        }
    }

    return (

        <div className="new-review">
            <form className="newReview" onSubmit={handleSubmit}>
                <fieldset> 
                    <div className="form-inner">
                        <legend className="new-review__title">Добавить свой отзыв</legend>
                        <div className="new-review__body">
                            <div className="new-review__inputs">  
                                <div>
                                    <input className="new-review__name-input" onInput={collectInputName} onClick={handleChangeText} type="text" placeholder="Имя и фамилия" value={reviewNameInput}  />

                                    <div className={`showinputErrors" ${errorForNameInput ? "hideinputErrors" : ''}`}> 
                                        {errorForNameInput} 
                                    </div>

                                </div>

                                <div>
                                    <input className="new-review__rating-input" onInput={collectInputRating} onClick={handleChangeText} type="text" placeholder="Оценка" value={reviewRatingInput} />
                                
                                    <div className={`"showinputErrors" ${errorForRatingInput ? "hideinputErrors" : ''}`}>
                                        {errorForRatingInput}
                                    </div>
                                    
                                </div>
                            </div>

                            <div className="new-review__textarea">
                                <textarea className="new-review__text-message" onInput={collectInputText}  placeholder="Текст отзыва" value={reviewTextArea}  />
                            </div>

                            <button className="reviewbutton" type="submit">Отправить отзыв</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    );
}

export default ReviewForm;