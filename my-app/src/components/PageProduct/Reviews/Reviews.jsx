import React from 'react';
import './Reviews.css';
import Review from './Review/Review';
import review from './Review/ReviewArray'
import ReviewForm from './ReviewForm/ReviewForm';

function Reviews() {    
    return (
        <>
            <div className='container-review'>
                <div className="reviewheader">
                    <div className="reviewheader__text">
                        <div className="reviewheader__text1">Отзывы</div>
                        <div className="reviewheader__textcount">425</div>
                    </div>
                </div>
                <div className="reviewlist">
                    {review.map((oneReview, index) => {
                        index = oneReview.id;
                        return (
                            <div key={index}>
                                <Review review={oneReview} />
                                {index !== review.length - 1 && <div className='line-container'><div className="linebetween"></div></div>}
                            </div>
                        );
                    })}
                            
                </div>
                
                <ReviewForm />
            </div>

        </>
    );
}

export default Reviews;