import './Adb.css';

function Adb(props) {
    const { src } = props;

    return (
        <div className="advertising__list-frame">
            <iframe src={src} />
        </div>

    );
}

export default Adb;