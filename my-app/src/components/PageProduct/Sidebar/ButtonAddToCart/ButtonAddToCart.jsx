import { React, useState} from "react";
import { useSelector, useDispatch } from "react-redux";
import { addProduct, removeProduct } from "./cart-reducer";
import './ButtonAddToCart.css';

function ButtonAddToCart(props) {
    const {product} = props;
    
    const [cartValue, setCartValue] = useState (() => {
        return localStorage.getItem('itemsInCart' || '');
    }); 

    let products = useSelector((store) => store.cart.products);
    
    const dispatch = useDispatch();
    
    const alreadyInCart = products.some((prevProduct) => {return prevProduct.id === product.id});
    let items = products.length;  

  

        function handleAddProduct(item, product) {
                dispatch(addProduct(product));
                ++items;
                localStorage.setItem('itemsInCart', items);
                setCartValue(items);
            }

        function handleRemoveProduct(item, product) {
                dispatch(removeProduct(product));
                localStorage.removeItem('itemsInCart');
                setCartValue('');
        }
        

        
        return (

                <div className="important__button">

                    {(alreadyInCart || cartValue) ? (
                        <div 
                        onClick={(item) => handleRemoveProduct(item, product)}
                        className="important__button important__button-text inactive-button">Товар уже в корзине 
                        </div>
                    ) : (
                        <div 
                        onClick={(item) => handleAddProduct(item, product)}
                        className="important__button important__button-text">Добавить в корзину
                        </div>
                    )}
                   
                </div>

        )
    
};

export default ButtonAddToCart;


