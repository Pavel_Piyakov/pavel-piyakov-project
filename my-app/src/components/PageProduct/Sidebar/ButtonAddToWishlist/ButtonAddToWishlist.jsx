import { React, useState} from "react";
import { useSelector, useDispatch } from "react-redux";
import { addProduct, removeProduct } from "./wishlist-reducer";
import './ButtonAddToWishlist.css';

function ButtonAddToWishlist(props) {
    const {product} = props;
    
    const [wishlistValue, setWishlistValue] = useState (() => {
        return localStorage.getItem('itemsInWishlist' || '');
    }); 

    let products = useSelector((store) => store.wishlist.products);
    
    const dispatch = useDispatch();
    
    const alreadyInWishlist = products.some((prevProduct) => {return prevProduct.id === product.id});
    let items = products.length;  

  

        function handleAddProduct(item, product) {
                dispatch(addProduct(product));
                ++items;
                localStorage.setItem('itemsInWishlist', items);
                setWishlistValue(items);
            }

        function handleRemoveProduct(item, product) {
                dispatch(removeProduct(product));
                localStorage.removeItem('itemsInWishlist');
                setWishlistValue('');
        }



        
        return (

            <div className="important__heart-icon">
            {(alreadyInWishlist || wishlistValue) ? (
                <div 
                onClick={(item) => handleRemoveProduct(item, product)}
                className="heart2 heartbody">
                </div>
            ) : (
                <div 
                onClick={(item) => handleAddProduct(item, product)}
                className="heart1">
                </div>
            )}
           
        </div>

        )
    
};

export default ButtonAddToWishlist;


