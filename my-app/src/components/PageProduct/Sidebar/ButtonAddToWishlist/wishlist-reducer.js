import { createSlice } from "@reduxjs/toolkit";

export const wishlistSlice = createSlice ({
    name: "wishlist",

    initialState: {
        products: [],
    },

    reducers: {
        addProduct: (prevState, action) => {
            const product = action.payload
            const alreadyInWishlist = prevState.products.some((prevProduct) => prevProduct.id);

            if (alreadyInWishlist) {
                return {
                    ...prevState
                };
            }
            
            else {
                return {
                ...prevState,
                products: [...prevState.products, product],
                };
            }

        },



        removeProduct: (prevState, action) => {
            const product = action.payload;
                return {...prevState, products: prevState.products.filter((prevProduct) => {
                    return prevProduct.id !== product.id;
                })
            }

        }
    },
});

export const { addProduct, removeProduct } = wishlistSlice.actions;
export default wishlistSlice.reducer;
