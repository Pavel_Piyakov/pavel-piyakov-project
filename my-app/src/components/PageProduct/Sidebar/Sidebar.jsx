import ADBlock from "./ADBlock/ADBlock";
import ButtonAddToCart from "./ButtonAddToCart/ButtonAddToCart";
import ButtonAddToWishlist from "./ButtonAddToWishlist/ButtonAddToWishlist";
import "./Sidebar.css";

function Sidebar() {
    return(
        <div className="sidebar">
            <div className="important">
                <div className="important__price-row">
                    <div className="important__price">
                        <div className="important__price-old">
                                <div>
                                    <s>75 990₽</s>
                                </div>

                                 <div className="important__price-sale">
                                    <div>-8%</div>
                                </div>
                        </div>


                        <div className="important__price-new">
                            <b>67 990₽</b>
                        </div>                      
                    </div>
                    
                    <ButtonAddToWishlist product = {{id: 4884}}  />
                </div>

                <div className="important__delivery">
                    <div>
                        Самовывоз в четверг, 1 сентября — <b>бесплатно</b>
                    </div>

                    <div>
                        Курьером в четверг, 1 сентября — <b>бесплатно</b>
                    </div>
                </div>
                <ButtonAddToCart product = {{id: 4884}}  />
            </div>
            <ADBlock />
        </div>
    )
};

export default Sidebar;