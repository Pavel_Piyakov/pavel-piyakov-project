import{ configureStore } from "@reduxjs/toolkit";
import cartReducer from '../src/components/PageProduct/Sidebar/ButtonAddToCart/cart-reducer'
import wishlistReducer from "./components/PageProduct/Sidebar/ButtonAddToWishlist/wishlist-reducer";


const logger = (store) => (next) => (action) => {
    console.log("action", action)

    let result = next(action);

    console.log("next state", store.getState())
    return result;
};

export const store = configureStore ({
    reducer: {
        cart: cartReducer,
        wishlist: wishlistReducer
    },

    middleware: [logger]
});